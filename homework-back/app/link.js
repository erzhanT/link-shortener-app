const express = require('express');
const {nanoid} = require('nanoid');
const Link = require("../models/Link");



const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const links = await Link.find()
        res.send(links);

    } catch (e) {
        res.sendStatus(500)
    }
});

router.get('/:id', async (req, res) => {
    try {

        const link = await Link.findOne({shortLink: req.params.id})

        if (link) {
            // res.send(link);
            res.status(301).redirect(link.originalLink)
        } else {
            res.sendStatus(404)
        }
    } catch (e) {
        res.sendStatus(500)
    }
});

router.post('/', async (req, res) => {
    try {
        const link = new Link({
            originalLink: req.body.originalUrl,
            shortLink: nanoid(6)
        });
        await link.save();
        res.send(link.shortLink);
    } catch (e) {
        res.sendStatus(500);
    }


});

module.exports = router;