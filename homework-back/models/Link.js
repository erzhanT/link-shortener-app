const mongoose = require('mongoose');

const LinkSchema = new mongoose.Schema({
    shortLink: String,
    originalLink: {
        type: String,
        required: true,
    }
});

const Link = mongoose.model('Link', LinkSchema);
module.exports = Link;