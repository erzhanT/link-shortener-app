const express = require('express');
const cors = require("cors");
const mongoose = require('mongoose')
const link = require('./app/link');
const exitHook = require('async-exit-hook');


const app = express();
app.use(express.json());
app.use(cors());

const port = 8001;

app.use('/link', link);

const run = async () => {
    await mongoose.connect('mongodb://localhost/link', {useNewUrlParser: true, useUnifiedTopology: true});

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
    exitHook(async callback => {
        await mongoose.disconnect();
        console.log('mongoose disconnected');
        callback();
    });

}

run().catch(console.error);