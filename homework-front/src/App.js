import React, {useState} from "react";
import './App.css';
import {useDispatch, useSelector} from "react-redux";
import {postLink} from "./store/actions";

const App = () => {

    const dispatch = useDispatch();
    const state = useSelector(state => state);

    const [link, setLink] = useState({
        originalUrl: ''
    });


    const createShortLink = (e) => {
        e.preventDefault();
        dispatch(postLink(link));
        setLink({
            originalUrl: '',
        });
    }

    const onChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;

        setLink(prevState => ({
            ...prevState,
            [name]: value
        }));
    }
    return (
        <>
            <h1>Shorten your link!</h1>
            <form onSubmit={createShortLink}>
            <input type="text"
                   name="originalUrl"
                   className="field"
                   placeholder="enter your link"
                   value={link.originalUrl}
                   onChange={onChange}
            />
            <button type="submit" className="btn">Shorten</button>
            </form>
                <>
                    <p className="description">Your link now look like this:</p>
                    <a href={`http://localhost:8001/link/${state.shortLink}`}>{"http://localhost:8001/" + state.shortLink}</a>
                </>
        </>
    );
};

export default App;
