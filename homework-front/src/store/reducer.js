import {CREATE_LINK} from "./actions";

const initialState = {
    shortLink: '',
    originalLink: ''
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_LINK :
            return {...state , shortLink: action.link}
        default:
            return state;
    }
}

export default reducer;