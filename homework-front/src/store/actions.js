import axiosApi from "../axiosApi";


export const CREATE_LINK = 'CREATE_LINK';



export const createLink = link => ({type: CREATE_LINK, link});

export const postLink = link => {
    return async dispatch => {
        try {
            const response = await axiosApi.post('/link', link);
            dispatch(createLink(response.data))
        } catch (e) {
            console.log(e)
        }
    }
}